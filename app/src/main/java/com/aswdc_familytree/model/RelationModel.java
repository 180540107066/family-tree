package com.aswdc_familytree.model;

import java.io.Serializable;

public class RelationModel implements Serializable {

    int RelationId;
    String RelationName;

    public int getRelationId() {
        return RelationId;
    }

    public void setRelationId(int relationId) {
        RelationId = relationId;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }

    @Override
    public String toString() {
        return "RelationModel{" +
                "RelationId=" + RelationId +
                ", RelationName='" + RelationName + '\'' +
                '}';
    }
}
