package com.aswdc_familytree.model;

import java.io.Serializable;

public class RelationWithUserModel implements Serializable {

    int Id;
    int UserId;
    int RelationId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getRelationId() {
        return RelationId;
    }

    public void setRelationId(int relationId) {
        RelationId = relationId;
    }

    @Override
    public String toString() {
        return "RelationWithUserModel{" +
                "Id=" + Id +
                ", UserId=" + UserId +
                ", RelationId=" + RelationId +
                '}';
    }
}
