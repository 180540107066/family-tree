package com.aswdc_familytree.model;

import java.io.Serializable;

public class UserAddModel implements Serializable {

    int UserId;
    String Name;
    String FatherName;
    String SurName;
    int Gender;
    String BirthDate;
    String Photo;
    String BirthPlace;
    int IsLive;
    String DeathDate;
    String DeathPlace;
    int RelationId;
    String MessageNote;
    String Relation;

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getBirthPlace() {
        return BirthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        BirthPlace = birthPlace;
    }

    public int getIsLive() {
        return IsLive;
    }

    public void setIsLive(int isLive) {
        IsLive = isLive;
    }

    public String getDeathDate() {
        return DeathDate;
    }

    public void setDeathDate(String deathDate) {
        DeathDate = deathDate;
    }

    public String getDeathPlace() {
        return DeathPlace;
    }

    public void setDeathPlace(String deathPlace) {
        DeathPlace = deathPlace;
    }

    public String getMessageNote() {
        return MessageNote;
    }

    public void setMessageNote(String messageNote) {
        MessageNote = messageNote;
    }

    public int getRelationId() {
        return RelationId;
    }

    public void setRelationId(int relationId) {
        RelationId = relationId;
    }

    @Override
    public String toString() {
        return "UserAddModel{" +
                "UserId=" + UserId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Gender=" + Gender +
                ", BirthDate='" + BirthDate + '\'' +
                ", Photo='" + Photo + '\'' +
                ", BirthPlace='" + BirthPlace + '\'' +
                ", IsLive=" + IsLive +
                ", DeathDate='" + DeathDate + '\'' +
                ", DeathPlace='" + DeathPlace + '\'' +
                ", RelationId=" + RelationId +
                ", MessageNote='" + MessageNote + '\'' +
                ", Relation='" + Relation + '\'' +
                '}';
    }
}
