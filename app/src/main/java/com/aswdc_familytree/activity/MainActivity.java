package com.aswdc_familytree.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.BuildConfig;
import com.aswdc_familytree.R;
import com.aswdc_familytree.adapter.FamilyMemberTreeAdapter;
import com.aswdc_familytree.adapter.RelationListAdapter;
import com.aswdc_familytree.database.TblUserAdd;
import com.aswdc_familytree.model.RelationModel;
import com.aswdc_familytree.model.RelationWithUserModel;
import com.aswdc_familytree.model.UserAddModel;
import com.aswdc_familytree.util.Constant;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {


    @BindView(R.id.familyMemberRelationRecyclerView)
    RecyclerView familyMemberRelationRecyclerView;
    @BindView(R.id.mainActivityImageView)
    ImageView mainActivityImageView;
    @BindView(R.id.mainActivityTextView)
    TextView mainActivityTextView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //    @BindView(R.id.btn_AddUser)
//    Button btnAddUser;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawerMainActivity)
    DrawerLayout drawerMainActivity;
    //    @BindView(R.id.familyMemberRelationSpinner)
//    Spinner familyMemberRelationSpinner;
    @BindView(R.id.mainActivityNoDataFound)
    LinearLayout mainActivityNoDataFound;
    @BindView(R.id.btn_AddUser)
    FloatingActionButton btnAddUser;
    @BindView(R.id.AddPerson)
    LinearLayout AddPerson;
    /*@BindView(R.id.tvUserName)
    TextView navUserName;
    @BindView(R.id.ivUserImage)
    ImageView navUserImage;*/

    private ActionBarDrawerToggle mToggle;
    Button btnAddUse;
    private boolean doubleBackToExitPressedOnce;


    ArrayList<UserAddModel> familyMemberRelationList = new ArrayList<>();
    FamilyMemberTreeAdapter familyMemberTreeAdapter;
    ArrayList<RelationModel> relationList = new ArrayList<>();
    RelationListAdapter relationListAdapter;
    int current = 0;
    ArrayList<RelationWithUserModel> relationWithUserList = new ArrayList<>();

    MyAddNewUserReceiver receiver = new MyAddNewUserReceiver();

    ArrayList<UserAddModel> userAddModels = new ArrayList<>();
    UserAddModel userAddModel;
    Dialog dialog;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpActionBar(getString(R.string.lbl_family_Tree), false);

        setNavigationView();
        btnAddUser = findViewById(R.id.btn_AddUser);
        setAdapter();
        CheckAndVisibleView();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constant.ADD_NEW_USER_FILTER));
    }

    public void setNavigationView() {
        DrawerLayout drawerMainActivity = findViewById(R.id.drawerMainActivity);
        mToggle = new ActionBarDrawerToggle(this, drawerMainActivity, R.string.open, R.string.close);
        drawerMainActivity.addDrawerListener(mToggle);
        mToggle.syncState();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        mToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        TextView userName;
        ImageView userImage;

        userName = findViewById(R.id.tvUserName);
        userImage = findViewById(R.id.ivUserImage);

//        void getDataForPersonalDetails(){
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userAddModel = (UserAddModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            if (userAddModel.getUserId() == 16) {
                userName.setText(userAddModel.getName() + " " + userAddModel.getFatherName() + " " + userAddModel.getSurName());
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    //    This navigation drawer menu.
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //    This navigation drawer menu.
    @SuppressLint("ShowToast")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem Item) {

        int id = Item.getItemId();
        if (id == R.id.menu_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        if (id == R.id.menu_familyMember) {
            Intent intent = new Intent(MainActivity.this, FamilyMember.class);
            startActivity(intent);
        }
        if (id == R.id.menu_share) {
            try {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "share app");
                String shareMessage = "https://play.google.com/stroe/apps/details?=" + BuildConfig.APPLICATION_ID + "\n\n";
                intent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(intent, "Share app"));
            } catch (Exception e) {
                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT);
            }
        }
        if (id == R.id.menu_feedback) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            /*Intent mailClient = new Intent(Intent.ACTION_VIEW);
            mailClient.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
            startActivity(mailClient);*/
            /*Intent i = new Intent(Intent.ACTION_SEND);

            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"app@familyTree.in"});
            i.putExtra(Intent.EXTRA_SUBJECT, "Feedback to Family Tree app .");
            i.putExtra(Intent.EXTRA_TEXT   , " ");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }*/
        }
        if (id == R.id.menu_exit) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
            finish();
        }
        return false;
    }

    void setAdapter() {

        familyMemberRelationRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        familyMemberRelationList.addAll(new TblUserAdd(this).getUserList());
        familyMemberTreeAdapter = new FamilyMemberTreeAdapter(this, familyMemberRelationList);
        familyMemberTreeAdapter.setOnRelationClickListener(position -> {
            familyMemberTreeAdapter.notifyDataSetChanged();
            familyMemberTreeAdapter.notifyItemInserted(position);
           /* Intent intent=new Intent(MainActivity.this,AddPersonActivity.class);
            intent.putExtra(Constant.USER_OBJECT,familyMemberRelationList.get(position));
            startActivity(intent);*/
//                private void showDialog() {

            /*new  Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
*/

            final Dialog dialog = new Dialog(MainActivity.this, R.style.BottomSheetDialogTheme);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.bottom_alert_dialog);

            LinearLayout cvEdit = dialog.findViewById(R.id.cvEdit);
            LinearLayout cvDelete = dialog.findViewById(R.id.cvDelete);
            LinearLayout cvView = dialog.findViewById(R.id.cvView);

            View bottomSheetView = LayoutInflater.from(getApplicationContext()).inflate(
                    R.layout.bottom_alert_dialog, (LinearLayout) findViewById(R.id.bottomSheetContainer)
            );

            cvEdit.setOnClickListener(v -> {
//                Toast.makeText(MainActivity.this,"Edit",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, AddPersonActivity.class);
                intent.putExtra(Constant.USER_OBJECT, familyMemberRelationList.get(position));
                startActivity(intent);
                dialog.dismiss();
            });

            cvDelete.setOnClickListener(v -> {
//                            Toast.makeText(MainActivity.this,"123",Toast.LENGTH_SHORT).show();
                showAlertDialog(position);
                dialog.dismiss();
            });

            dialog.show();
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = R.style.dialogStyle;
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            cvView.setOnClickListener(v -> {
//                            Toast.makeText(MainActivity.this,"123",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, personal_information.class);
                intent.putExtra(Constant.USER_OBJECT, familyMemberRelationList.get(position));
                startActivity(intent);
                dialog.dismiss();

            });
              /*  }
            },500);*/
        });
        familyMemberRelationRecyclerView.setAdapter(familyMemberTreeAdapter);

    }


    void showAlertDialog(int position) {

        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.custom_dialog);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.view_border));
        }
//        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animation;

        Button yes = dialog.findViewById(R.id.btnYesDialog);
        Button no = dialog.findViewById(R.id.btnNoDialog);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int deleteUserId = new TblUserAdd(MainActivity.this).deleteUserById(familyMemberRelationList.get(position).getUserId());
                if (deleteUserId > 0) {
                    Toast.makeText(MainActivity.this, "Delete User Successfully", Toast.LENGTH_SHORT).show();
                    familyMemberRelationList.remove(position);
                    familyMemberTreeAdapter.notifyItemRemoved(position);
                    familyMemberTreeAdapter.notifyItemRangeChanged(0, familyMemberRelationList.size());
                    CheckAndVisibleView();
                } else {
                    Toast.makeText(MainActivity.this, "Something Went to wrong", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();

            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
        /*AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setIcon(R.drawable.ic_baseline_warning_24);
        alertDialog.setTitle("Confirm");
        alertDialog.setMessage("Are you sure want to delete ? ");

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deleteUserId = new TblUserAdd(this).deleteUserById(familyMemberRelationList.get(position).getUserId());
                    if (deleteUserId > 0) {
                        Toast.makeText(this, "Delete User Successfully", Toast.LENGTH_SHORT).show();
                        familyMemberRelationList.remove(position);
                        familyMemberTreeAdapter.notifyItemRemoved(position);
                        familyMemberTreeAdapter.notifyItemRangeChanged(0, familyMemberRelationList.size());
                        CheckAndVisibleView();
                    } else {
                        Toast.makeText(this, "Something Went to wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();*/
    }

    private void sendAddNewUserBroadCast(int userId) {
        Intent intent = new Intent(Constant.ADD_NEW_USER_FILTER);
        intent.putExtra(Constant.USER_ID, userId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    //     This method set visible view data found or not found.
    void CheckAndVisibleView() {
        if (familyMemberRelationList.size() > 0) {
            btnAddUser.setVisibility(View.VISIBLE);
            mainActivityImageView.setVisibility(View.GONE);
            mainActivityTextView.setVisibility(View.GONE);
            drawerMainActivity.setVisibility(View.VISIBLE);
            mainActivityNoDataFound.setVisibility(View.GONE);
            familyMemberRelationRecyclerView.setVisibility(View.VISIBLE);
            AddPerson.setVisibility(View.VISIBLE);
        } else {
            btnAddUser.setVisibility(View.VISIBLE);
            mainActivityImageView.setVisibility(View.VISIBLE);
            mainActivityTextView.setVisibility(View.VISIBLE);
            drawerMainActivity.setVisibility(View.VISIBLE);
            mainActivityNoDataFound.setVisibility(View.VISIBLE);
            familyMemberRelationRecyclerView.setVisibility(View.GONE);
            AddPerson.setVisibility(View.VISIBLE);
        }
    }


    //      this code exist app on onBackPress
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please", Toast.LENGTH_SHORT).show();
        finish();
        new Handler().postDelayed(() -> {

        }, 2000);
    }

    @OnClick(R.id.btn_AddUser)
    public void onViewClicked() {
        Intent intent = new Intent(MainActivity.this, AddPersonActivity.class);
        startActivity(intent);
    }


    class MyAddNewUserReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(MainActivity.this, "  " + intent.hasExtra(Constant.USER_ID), Toast.LENGTH_SHORT).show();
            if (intent.hasExtra(Constant.USER_ID)) {
                Intent i = getIntent();
                int userId = i.getIntExtra("UserId", 0);
                familyMemberTreeAdapter.notifyDataSetChanged();
                familyMemberTreeAdapter.notifyItemInserted(userId);
            }
        }
    }
}