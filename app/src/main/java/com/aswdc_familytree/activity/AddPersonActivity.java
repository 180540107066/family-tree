package com.aswdc_familytree.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aswdc_familytree.R;
import com.aswdc_familytree.adapter.FamilyMemberListAdapter;
import com.aswdc_familytree.adapter.RelationListAdapter;
import com.aswdc_familytree.database.Relation;
import com.aswdc_familytree.database.TblUserAdd;
import com.aswdc_familytree.model.RelationModel;
import com.aswdc_familytree.model.UserAddModel;
import com.aswdc_familytree.util.Constant;
import com.aswdc_familytree.util.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddPersonActivity<setOnClick> extends AppCompatActivity {

    private static final int STORAGE_PERMISSION_CODE = 1;
//    @BindView(R.id.too)
//    Toolbar toolbar;
    @BindView(R.id.addPerson_name)
    EditText addPersonName;
    @BindView(R.id.addPerson_fatherName)
    EditText addPersonFatherName;
    @BindView(R.id.addPerson_surName)
    EditText addPersonSurName;
    @BindView(R.id.addPerson_rbMale)
    RadioButton addPersonRbMale;
    @BindView(R.id.addPerson_rbFemale)
    RadioButton addPersonRbFemale;
    @BindView(R.id.addPerson_rbOther)
    RadioButton addPersonRbOther;
    @BindView(R.id.addPerson_imagePhoto)
    ImageView addPersonImagePhoto;
    @BindView(R.id.addPerson_birthDate)
    EditText addPersonBirthDate;
    @BindView(R.id.addPerson_birthPlace)
    EditText addPersonBirthPlace;
    @BindView(R.id.addPerson_switchIsLive)
    SwitchCompat addPersonSwitchIsLive;
    @BindView(R.id.addPerson_deathDate)
    EditText addPersonDeathDate;
    @BindView(R.id.addPerson_deathPlace)
    EditText addPersonDeathPlace;
    @BindView(R.id.addPerson_messageNote)
    EditText addPersonMessageNote;

    String starting = "1995-07-19T12:00:00";

    @BindView(R.id.addPerson_lblDeathDate)
    TextView addPersonLblDeathDate;

    ArrayList<UserAddModel> familyMemberList = new ArrayList<>();
    FamilyMemberListAdapter familyMemberListAdapter;
    ArrayList<RelationModel> relationList = new ArrayList<>();
    RelationListAdapter relationListAdapter;
    UserAddModel userAddModel;
    //
    //   permission camera and storage pick photos
    private static final int CAMERA_REQUEST_CODE = 1000;
    private static final int STORAGE_REQUEST_CODE = 1001;
    private static final int IMAGE_PICK_CAMERA_CODE = 1002;
    private static final int IMAGE_PICK_GALLERY_CODE = 1003;
    @BindView(R.id.addPerson_saveButton)
    Button addPersonSaveButton;
    Uri uri;


    @BindView(R.id.spFamilyRelationList)
    Spinner spFamilyRelationList;
    private String[] cameraPermissions; //camera and storage.
    private String[] storagePermission;// storage.
    private Uri imageUri;
    private ContentValues Values;

    File srcDir;
    File desDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);
        ButterKnife.bind(this);
        setUpActionBar(true, "Add Person");
        setAddPersonBirthDate();
        setAddPersonDeathDate();
//        onAddPersonImagePhotoClicked();
//        permission arrays
        cameraPermissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};


        getDataForUpdate();
        onChecked();
    }

    //      This toolbar set activity
    @SuppressLint("ResourceType")
    public void setUpActionBar(boolean isBackArrow, String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddPersonActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    //    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(myAddNewUserReceiver);
//    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //  this method for set date.
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    void setAddPersonBirthDate() {
        final Calendar newCalendar = Calendar.getInstance();
        addPersonBirthDate.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR)));
        setSpFamilyRelationAdapter();
    }

    //  this method for set date.
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    void setAddPersonDeathDate() {
        final Calendar newCalendar = Calendar.getInstance();
        addPersonDeathDate.setText(String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                String.format("%02d", newCalendar.get(Calendar.YEAR)));

    }

    //     This code for open user camera chose user photos
    @OnClick(R.id.addPerson_imagePhoto)
    public void onAddPersonImagePhotoClicked() {
//        CropImage.startPickImageActivity(AddPersonActivity.this);
        imagePickDialog();
    }

    private void imagePickDialog() {
        String[] options = {"Camera", "Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Photo From");
        builder.setItems(options, (dialog, which) -> {
            if (which == 0) {
                if (!checkCameraPermission()) {
                    requestCameraPermission();
                } else {
                    pickFromCamera();
                }
            } else if (which == 1) {
                if (!checkStoragePermission()) {
                    requestStoragePermission();
                } else {
                    pickFromGallery();
                }
            }
        });

        builder.create().show();
    }

    private void pickFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickFromCamera() {
        ContentValues cv = new ContentValues();
        cv.put(MediaStore.Images.Media.TITLE, "Image title");
        cv.put(MediaStore.Images.Media.DESCRIPTION, "Image description");

        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cv);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, storagePermission, STORAGE_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result2 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result2;

    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermissions, CAMERA_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

//        result of permission allow/denied
        switch (requestCode) {
            case CAMERA_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cameraAccepted && storageAccepted) {
                        pickFromCamera();
                    } else {
                        Toast.makeText(this, "Camera and Storage permissions are required", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case STORAGE_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (storageAccepted) {
                        pickFromGallery();
                    } else {
                        Toast.makeText(this, "Storage permissions is required", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
        }
    }

    // this
    private void copyFileDirectory(String srcDir, String desDir) {
        try {
            File src = new File(srcDir);
            File des = new File(desDir);
            if (src.isDirectory()) {
                String[] files = src.list();
                int fileLength = files.length;
                for (String file : files) {
                    String src1 = new File(src, file).getPath();
                    String dst1 = des.getPath();

//                    copyFileDirectory(src1,dst1);
                }
            } else {
                copyFile(src, des);
            }
        } catch (Exception e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void copyFile(File srcDir, File desDir) throws IOException {
        if (!desDir.getParentFile().exists()) {
            desDir.mkdirs();
        }
        if (!desDir.exists()) {
            desDir.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(srcDir).getChannel();
            destination = new FileOutputStream(desDir).getChannel();
            destination.transferFrom(source, 0, source.size());

//            save image
            imageUri = Uri.parse(desDir.getPath());
            addPersonImagePhoto.setImageURI(imageUri);
            Log.d("ImagePath", "copyFile" + imageUri);
        } catch (Exception e) {
            Toast.makeText(this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode == RESULT_OK) {

            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                assert data != null;
                CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1).start(this);

            } else if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1).start(this);
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
//                    assert result != null;
                    Uri resultUri = result.getUri();
                    imageUri = resultUri;

                    addPersonImagePhoto.setImageURI(resultUri);
//                    addPersonImagePhoto.setImageResource(((int) resultUri));
                    copyFileDirectory("" + imageUri.getPath(), "" + getDir("SQLiteRecordImage", MODE_PRIVATE));
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /*public void insertImg(int id , Bitmap img ) {


        byte[] data = getBitmapAsByteArray(img); // this is a function

        insertStatement_logo.bindLong(1, id);
        insertStatement_logo.bindBlob(2, data);

        insertStatement_logo.executeInsert();
        insertStatement_logo.clearBindings() ;

    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }*/

    void setSpFamilyRelationAdapter() {
        relationList.addAll(new Relation(this).getRelation());
        relationListAdapter = new RelationListAdapter(this, relationList);
        spFamilyRelationList.setAdapter(relationListAdapter);
    }

    int getSelectedPositionFromRelationId(int relationId) {
        for (int i = 0; i < relationList.size(); i++) {
            if (relationList.get(i).getRelationId() == relationId) {
                return i;
            }
        }
        return 0;
    }

    // This code for DatePicker dialog open chose date
    @OnClick(R.id.addPerson_birthDate)
    public void onAddPersonBirthDateClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(starting);
        newCalendar.setTimeInMillis(date.getTime());
        @SuppressLint({"SetTextI18n", "DefaultLocale"}) DatePickerDialog datePickerDialog = new DatePickerDialog(AddPersonActivity.this,
                (datePicker, year, month, day_of_month) -> addPersonBirthDate.setText(String.format("%02d", day_of_month) + "/"
                        + String.format("%02d", (month + 1)) + "/"
                        + year), newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    // This code for DatePicker dialog open chose date
    @OnClick(R.id.addPerson_deathDate)
    public void onAddPersonDeathDateClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(starting);
        newCalendar.setTimeInMillis(date.getTime());
        @SuppressLint({"DefaultLocale", "SetTextI18n"}) DatePickerDialog datePickerDialog = new DatePickerDialog(AddPersonActivity.this,
                (datePicker, year, month, day_of_month) -> addPersonDeathDate.setText(String.format("%02d", day_of_month) + "/"
                        + String.format("%02d", (month + 1)) + "/"
                        + year), newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userAddModel = (UserAddModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            Objects.requireNonNull(getSupportActionBar()).setTitle("Edit Details");
            addPersonName.setText(userAddModel.getName());
            addPersonFatherName.setText(userAddModel.getFatherName());
            addPersonSurName.setText(userAddModel.getSurName());
            if (userAddModel.getGender() == Constant.MALE) {
                addPersonRbMale.setChecked(true);
            } else if (userAddModel.getGender() == Constant.FEMALE) {
                addPersonRbFemale.setChecked(true);
            } else {
                addPersonRbOther.setChecked(true);
            }
            if (userAddModel.getIsLive() == Constant.IsLive) {
                addPersonSwitchIsLive.setChecked(true);
            } else {
                addPersonSwitchIsLive.setChecked(false);
            }
//            addPersonImagePhoto.setImageURI(userAddModel.getPhoto());
//            addPersonImagePhoto.setId(userAddModel.getPhoto());
            addPersonBirthDate.setText(userAddModel.getBirthDate());
            addPersonBirthPlace.setText(userAddModel.getBirthPlace());
            addPersonDeathDate.setText(userAddModel.getDeathDate());
            addPersonDeathPlace.setText(userAddModel.getDeathPlace());
            addPersonMessageNote.setText(userAddModel.getMessageNote());
//            Log.d("run::1::","",userAddModel.getRelationId());
            spFamilyRelationList.setSelection(getSelectedPositionFromRelationId(userAddModel.getRelationId()));

        }


    }


    @OnClick(R.id.addPerson_saveButton)
    public void onViewClicked() {
        if (isValidUser()) {
            if (userAddModel == null) {

                long lastInsertUserId = new TblUserAdd(getApplicationContext()).insertUser(addPersonName.getText().toString(),
                        addPersonFatherName.getText().toString(), addPersonSurName.getText().toString(),
                        (addPersonRbMale.isChecked() ? Constant.MALE : (addPersonRbFemale.isChecked() ? Constant.FEMALE : Constant.OTHER)), addPersonBirthDate.getText().toString(),
                        addPersonImagePhoto.toString(), addPersonBirthPlace.getText().toString(), addPersonSwitchIsLive.isChecked() ? Constant.IsLive : Constant.IsNotLive,
                        addPersonDeathDate.getText().toString(), addPersonDeathPlace.getText().toString(), relationList.get(spFamilyRelationList.getSelectedItemPosition()).getRelationId()
                        , addPersonMessageNote.getText().toString());
                Toast.makeText(getApplicationContext(), lastInsertUserId > 0 ?
                        "User insert Successfully" : "Something went to wrong ", Toast.LENGTH_SHORT).show();
                sendAddNewUserBroadCast((int) lastInsertUserId);
            } else {
                long lastInsertUserId = new TblUserAdd(getApplicationContext()).updateUserById(addPersonName.getText().toString(), addPersonFatherName.getText().toString(),
                        addPersonSurName.getText().toString(), (addPersonRbMale.isChecked() ? Constant.MALE : (addPersonRbFemale.isChecked() ? Constant.FEMALE : Constant.OTHER)),
                        addPersonBirthDate.getText().toString(),
                        addPersonImagePhoto.toString(), addPersonBirthPlace.getText().toString(),
                        addPersonSwitchIsLive.isChecked() ? Constant.IsLive : Constant.IsNotLive, addPersonDeathDate.getText().toString(),
                        addPersonDeathPlace.getText().toString(),
                        relationList.get(spFamilyRelationList.getSelectedItemPosition()).getRelationId()
                        , addPersonMessageNote.getText().toString(), userAddModel.getUserId());
                Toast.makeText(getApplicationContext(), lastInsertUserId > 0 ?
                        "User Update Successfully" : "Something went to wrong ", Toast.LENGTH_SHORT).show();
                sendAddNewUserBroadCast((int) lastInsertUserId);
            }
            Intent intent = new Intent(AddPersonActivity.this, MainActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

//            finish();
        }

    }

    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(addPersonName.getText().toString().trim())) {
            isValid = false;
            addPersonName.setError("Enter Name");

        } else {
            String name = addPersonName.getText().toString().trim();
            String namePattern = "[A-Za-z]+";
            if (!(name.matches(namePattern))) {
                addPersonName.setError("Enter Valid Name");
                isValid = false;
            }
        }
        if (spFamilyRelationList.getSelectedItemPosition() == 0) {
            isValid = false;
            Toast.makeText(this, "Choose One Relation", Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }


    void sendAddNewUserBroadCast(int userId) {
        Intent intent = new Intent(Constant.ADD_NEW_USER_FILTER);
        intent.putExtra(Constant.USER_ID, userId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    //    Visibility for person isLive or / Not
    @OnClick(R.id.addPerson_switchIsLive)
    public void onChecked() {
        if (addPersonSwitchIsLive.isChecked()) {
            addPersonDeathDate.setVisibility(View.GONE);
            addPersonDeathPlace.setVisibility(View.GONE);
            addPersonLblDeathDate.setVisibility(View.GONE);
        } else {
            addPersonDeathDate.setVisibility(View.VISIBLE);
            addPersonDeathPlace.setVisibility(View.VISIBLE);
            addPersonLblDeathDate.setVisibility(View.VISIBLE);
        }
    }
}