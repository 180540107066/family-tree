package com.aswdc_familytree.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.R;
import com.aswdc_familytree.model.UserAddModel;
import com.aswdc_familytree.util.Constant;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class personal_information extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvGender)
    TextView tvGender;
    @BindView(R.id.tvBirthDate)
    TextView tvBirthDate;
    @BindView(R.id.tvBirthPlace)
    TextView tvBirthPlace;
    @BindView(R.id.tvDeathDate)
    TextView tvDeathDate;
    @BindView(R.id.tvDeathPlace)
    TextView tvDeathPlace;
    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @BindView(R.id.pdtvBirtDate)
    TextView pdtvBirthDate;
    @BindView(R.id.pdtvBirthPlace)
    TextView pdtvBirthPlace;
    @BindView(R.id.pdtvDeathDate)
    TextView pdtvDeathDate;
    @BindView(R.id.pdtvDeathPlace)
    TextView pdtvDeathPlace;
    @BindView(R.id.pdtvMessage)
    TextView pdtvMessage;
    /* @BindView(R.id.ivParentFather)
     ImageView ivParentFather;
     @BindView(R.id.tvFullNameFather)
     TextView tvFullNameFather;
     @BindView(R.id.ivMother)
     ImageView ivMother;
     @BindView(R.id.tvFullNameMother)
     TextView tvFullNameMother;
     @BindView(R.id.ivSiblings)
     ImageView ivSiblings;
     @BindView(R.id.tvFullNameSiblings)
     TextView tvFullNameSiblings;
     @BindView(R.id.revChildren)
     RecyclerView revChildren;
     @BindView(R.id.revSiblings)
     RecyclerView revSiblings;



 */
    ArrayList<UserAddModel> siblingsList = new ArrayList<>();
    RecyclerView rcvSiblingsList;
    ArrayList<UserAddModel> userAddModels = new ArrayList<>();
    UserAddModel userAddModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_infromation);
        ButterKnife.bind(this);
        setUpActionBar("Personal detail", true);
        getDataForPersonalDetails();
    }

    public void setUpActionBar(String title, boolean isBackArrow) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
    }

    //    This code for backArrows action control
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    void setAdapter() {
       /*  revSiblings.setLayoutManager(new GridLayoutManager(this, 1));
         rcvSiblingsList.addAll(new TblUserAdd(this).getUserList());
         adapter = new FamilyMemberListAdapter(this, rcvSiblingsList, position -> {
         familyMemberRecyclerView.setAdapter(adapter);*/
    }

    void getDataForPersonalDetails() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userAddModel = (UserAddModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            tvFullName.setText(userAddModel.getName() + " " + userAddModel.getFatherName() + " " + userAddModel.getSurName());
            if (userAddModel.getGender() == Constant.MALE) {
                tvGender.setText("Male");
            } else if (userAddModel.getGender() == Constant.FEMALE) {
                tvGender.setText("Female");
            } else {
                tvGender.setText("Others");
            }
            /*if ((userAddModel.getBirthDate().length()<0)){
                tvBirthDate.setVisibility(View.GONE);
                pdtvBirthDate.setVisibility(View.GONE);
            }
            if ((userAddModel.getBirthPlace().length()<0) ){
                tvBirthPlace.setVisibility(View.GONE);
                pdtvBirthPlace.setVisibility(View.GONE);
            }
            if ((userAddModel.getDeathDate().length()<0)){
                tvDeathDate.setVisibility(View.GONE);
                pdtvDeathDate.setVisibility(View.GONE);
            }
            if ((userAddModel.getDeathPlace().length()<0) ){
                tvDeathPlace.setVisibility(View.GONE);
                pdtvDeathPlace.setVisibility(View.GONE);
            }
            if ((userAddModel.getMessageNote().length()<0) ){
                tvMessage.setVisibility(View.GONE);
                pdtvMessage.setVisibility(View.GONE);
            }*/
            tvBirthDate.setText(userAddModel.getBirthDate());
            tvBirthPlace.setText(userAddModel.getBirthPlace());
            tvDeathDate.setText(userAddModel.getDeathDate());
            tvDeathPlace.setText(userAddModel.getDeathPlace());
            tvMessage.setText(userAddModel.getMessageNote());
        }
    }

  /*  @OnClick(R.id.revChildren)
    public void onRevChildrenClicked() {
    }

    @OnClick(R.id.revSiblings)
    public void onRevSiblingsClicked() {
    }*/
}
