package com.aswdc_familytree.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.R;
import com.aswdc_familytree.adapter.FamilyMemberListAdapter;
import com.aswdc_familytree.database.TblUserAdd;
import com.aswdc_familytree.model.UserAddModel;
import com.aswdc_familytree.util.Constant;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FamilyMember extends AppCompatActivity
//        implements PopupMenu.OnMenuItemClickListener
{


    @BindView(R.id.familyMemberRecyclerView)
    RecyclerView familyMemberRecyclerView;

    RecyclerView recyclerViewFamilyMemberList;
    ArrayList<UserAddModel> familyMemberList = new ArrayList<>();

    ImageView ivButton;
    //    @BindView(R.id.popUpMenu)
//    Button ivButton;
    FamilyMemberListAdapter adapter;
    @BindView(R.id.familyMemberImg)
    ImageView familyMemberImg;
    @BindView(R.id.familyMemberTextView)
    TextView familyMemberTextView;
    @BindView(R.id.familyMemberNoData)
    LinearLayout familyMemberNoData;

    MyAddNewUserReceiver myAddNewUserReceiver = new MyAddNewUserReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_member);
        ButterKnife.bind(this);
        setUpActionBar("Family member", true);
        setAdapter();
        CheckAndVisibleView();
        ivButton = findViewById(R.id.popUpMenu);
//        findViewById();
        customAlertDialog();

        LocalBroadcastManager.getInstance(this).registerReceiver(myAddNewUserReceiver, new IntentFilter(Constant.ADD_NEW_USER_FILTER));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myAddNewUserReceiver);
    }

    public void customAlertDialog() {
        ivButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(FamilyMember.this, "Image save", Toast.LENGTH_SHORT).show();
                String[] options = {"Images", "Pdf "};
                AlertDialog.Builder builder = new AlertDialog.Builder(FamilyMember.this);
                builder.setTitle("Save");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
//
                            Toast.makeText(FamilyMember.this, "Images", Toast.LENGTH_SHORT).show();

                        } else if (which == 1) {
//
                            Toast.makeText(FamilyMember.this, "Pdf file", Toast.LENGTH_SHORT).show();

                        }
                    }
                });

                builder.create().show();
            }

        });
    }

    public void setUpActionBar(String title, boolean isBackArrow) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
   /* @Override


    public void showPopup(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.imageSave:
//                onCilck();
                Toast.makeText(this, "Image save", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.pdfSave:
                Toast.makeText(this, "Pdf file", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }

    }
*/


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FamilyMember.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    //     This set Adapter in  familyMemberRecyclerView
    void setAdapter() {
        familyMemberRecyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        familyMemberList.addAll(new TblUserAdd(this).getUserList());
        adapter = new FamilyMemberListAdapter(this, familyMemberList, position -> {
          /*  Intent intent=new Intent(FamilyMember.this,AddPersonActivity.class);
            intent.putExtra(Constant.USER_OBJECT,familyMemberList.get(position));
            startActivity(intent);*/
        });
        familyMemberRecyclerView.setAdapter(adapter);
    }

    //      this visible for member or noDataFamily
    void CheckAndVisibleView() {
        if (familyMemberList.size() > 0) {
            familyMemberNoData.setVisibility(View.GONE);
            familyMemberRecyclerView.setVisibility(View.VISIBLE);
        } else {
            familyMemberNoData.setVisibility(View.VISIBLE);
            familyMemberRecyclerView.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.familyMemberRecyclerView)
    public void onViewClicked() {
    }


    class MyAddNewUserReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(FamilyMember.this, "User Add::" + intent.hasExtra(Constant.USER_ID), Toast.LENGTH_SHORT).show();

        }
    }


}