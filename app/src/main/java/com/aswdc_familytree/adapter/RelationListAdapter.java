package com.aswdc_familytree.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.aswdc_familytree.R;
import com.aswdc_familytree.model.RelationModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelationListAdapter extends BaseAdapter {

    Context context;
    ArrayList<RelationModel> relationList;

    public RelationListAdapter(Context context, ArrayList<RelationModel> relationList) {
        this.context = context;
        this.relationList = relationList;
    }

    @Override
    public int getCount() {
        return relationList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View v = view;
        ViewHolder viewHolder;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.view_row_spinner_text, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }
        viewHolder.relationName.setText(relationList.get(i).getRelationName());
        if (i == 0) {
            viewHolder.relationName.setTextColor(ContextCompat.getColor(context, R.color.gray));
        } else {
            viewHolder.relationName.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
        return v;
    }

    class ViewHolder {
        @BindView(R.id.relationName)
        TextView relationName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
