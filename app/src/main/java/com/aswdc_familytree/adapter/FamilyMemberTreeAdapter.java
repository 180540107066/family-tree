package com.aswdc_familytree.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.R;
import com.aswdc_familytree.model.UserAddModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FamilyMemberTreeAdapter extends RecyclerView.Adapter<FamilyMemberTreeAdapter.UserHolder> {

    Context context;
    ArrayList<UserAddModel> familyMemberRelationList;
    OnRelationClick onRelationClick;


    public FamilyMemberTreeAdapter(Context context, ArrayList<UserAddModel> familyMemberRelationList) {
        this.context = context;
        this.familyMemberRelationList = familyMemberRelationList;

    }

    public void setOnRelationClickListener(OnRelationClick onRelationClick) {
        this.onRelationClick = onRelationClick;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_family_member_tree, null));
    }

    //    @SuppressLint("SetTextI18n")
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
//        holder.viewRowMemberImageView.setId((familyMemberRelationList.get(position).getPhoto()));
//        holder.viewRowMemberImageView.setImageURI(Uri.parse(familyMemberRelationList.get(position).getPhoto()));
        holder.fullMemberNameTextView.setText(familyMemberRelationList.get(position).getName() + "  "
                + familyMemberRelationList.get(position).getFatherName() + "  "
                + familyMemberRelationList.get(position).getSurName());
        holder.tvRelationName.setText(familyMemberRelationList.get(position).getRelation());
        holder.itemView.setOnClickListener(v -> {
            if (onRelationClick != null) {
                onRelationClick.onClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return familyMemberRelationList.size();
    }

    public interface OnRelationClick {
        void onClick(int position);

    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewRowMemberImageView)
        ImageView viewRowMemberImageView;
        @BindView(R.id.fullMemberNameTextView)
        TextView fullMemberNameTextView;
        @BindView(R.id.tvRelationName)
        TextView tvRelationName;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
