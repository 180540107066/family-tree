package com.aswdc_familytree.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.model.UserAddModel;

import java.util.ArrayList;

public class RelationSiblingsListAdapter extends RecyclerView.Adapter<RelationSiblingsListAdapter.UserHolder> {
    Context context;
    ArrayList<UserAddModel> relationSiblingsList;

    public RelationSiblingsListAdapter(Context context, ArrayList<UserAddModel> relationSiblingsList) {
        this.context = context;
        this.relationSiblingsList = relationSiblingsList;
    }

    @NonNull
    @Override
    public RelationSiblingsListAdapter.UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RelationSiblingsListAdapter.UserHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class UserHolder extends RecyclerView.ViewHolder {
        public UserHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
