package com.aswdc_familytree.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc_familytree.R;
import com.aswdc_familytree.model.UserAddModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FamilyMemberListAdapter extends RecyclerView.Adapter<FamilyMemberListAdapter.UserHolder> {

    Context context;
    ArrayList<UserAddModel> familyMemberList;
    OnEditClickListener onEditClickListener;


    public FamilyMemberListAdapter(Context context, ArrayList<UserAddModel> familyMemberList, OnEditClickListener onEditClickListener) {
        this.context = context;
        this.familyMemberList = familyMemberList;
        this.onEditClickListener = onEditClickListener;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
//        holder.viewRowMemberImageView.setImageResource(familyMemberList.get(position).getPhoto());
//        holder.viewRowMemberImageView.setImageBitmap(familyMemberList.get(position).getPhoto());
        holder.fullMemberNameTextView.setText(familyMemberList.get(position).getName() + "  "
                + familyMemberList.get(position).getFatherName() + "  "
                + familyMemberList.get(position).getSurName());

        holder.itemView.setOnClickListener(v -> {
            if (onEditClickListener != null) {
                onEditClickListener.OnItemClick(position);
            }
        });
    }


    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_familyuserlist, null));
    }

    public interface OnEditClickListener {

        void OnItemClick(int position);
//        void onClickItem();

    }

    @Override
    public int getItemCount() {
        return familyMemberList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.viewRowMemberImageView)
        ImageView viewRowMemberImageView;
        @BindView(R.id.fullMemberNameTextView)
        TextView fullMemberNameTextView;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}

