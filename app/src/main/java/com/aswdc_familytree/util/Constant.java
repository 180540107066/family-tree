package com.aswdc_familytree.util;

public class Constant {
    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int OTHER = 3;

    public static final String Gender = "Gender";

    public static final String USER_ID = "UserId";
//    public static final int USER_ID = 1;

    public static final String USER_OBJECT = "UserObject";

    public static final int IsLive = 0;
    public static final int IsNotLive = 1;

    public static final String ADD_NEW_USER_FILTER = "com.aswdc_familytree.addnewuser";
}
