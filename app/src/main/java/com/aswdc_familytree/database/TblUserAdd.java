package com.aswdc_familytree.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_familytree.model.UserAddModel;

import java.util.ArrayList;

public class TblUserAdd extends MyDatabase {

    public static final String TABLE_NAME = "TblUserAdd";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String GENDER = "Gender";
    public static final String BIRTH_DATE = "BirthDate";
    public static final String PHOTO = "Photo";
    public static final String BIRTH_PLACE = "BirthPlace";
    public static final String IS_LIVE = "IsLive";
    public static final String DEATH_DATE = "DeathDate";
    public static final String DEATH_PLACE = "DeathPlace";
    public static final String RELATION_ID = "RelationId";
    public static final String MESSAGE_NOTE = "MessageNote";

    public static final String RELATION_Name = "RelationName";

    public TblUserAdd(Context context) {
        super(context);
    }

    public ArrayList<UserAddModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserAddModel> list = new ArrayList<>();
//        String query = " SELECT * FROM " + TABLE_NAME;
        String query = "SELECT " +
                " UserId," +
                " TblUserAdd.Name as Name," +
                " FatherName," +
                " SurName," +
                " Gender," +
                " BirthDate," +
                " Photo," +
                " BirthPlace, " +
                " IsLive," +
                " DeathDate, " +
                " DeathPlace," +
                " MessageNote," +
                " Relation.RelationId," +
                " Relation.RelationName as RelationName" +

                " FROM " +
                " TblUserAdd " +
                " INNER JOIN  Relation ON TblUserAdd.RelationId = Relation.RelationId";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
//        Log.d("UserList:1:", "" + cursor.getCount());
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    //        This UserModel create for reuse code.
    public UserAddModel getCreateModelUsingCursor(Cursor cursor) {
        UserAddModel model = new UserAddModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        model.setBirthDate(cursor.getString(cursor.getColumnIndex(BIRTH_DATE)));
        model.setBirthPlace(cursor.getString(cursor.getColumnIndex(BIRTH_PLACE)));
        model.setDeathDate(cursor.getString(cursor.getColumnIndex(DEATH_DATE)));
        model.setDeathPlace(cursor.getString(cursor.getColumnIndex(DEATH_PLACE)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        model.setIsLive(cursor.getInt(cursor.getColumnIndex(IS_LIVE)));
        model.setMessageNote(cursor.getString(cursor.getColumnIndex(MESSAGE_NOTE)));
        model.setPhoto(cursor.getString(cursor.getColumnIndex(PHOTO)));
        model.setRelationId(cursor.getInt(cursor.getColumnIndex(RELATION_ID)));
        model.setRelation(cursor.getString(cursor.getColumnIndex(RELATION_Name)));
        return model;
    }

    //         select query for insert user for database
    public long insertUser(String name, String fatherName, String surName, int gender, String birthDate,
                           String photo, String birthPlace, int isLive, String deathDate, String deathPlace, int relationId, String messageNote) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(GENDER, gender);
        cv.put(BIRTH_DATE, birthDate);
        cv.put(PHOTO, String.valueOf(photo));
        cv.put(BIRTH_PLACE, birthPlace);
        cv.put(IS_LIVE, isLive);
        cv.put(DEATH_DATE, deathDate);
        cv.put(DEATH_PLACE, deathPlace);
        cv.put(RELATION_ID, relationId);
        cv.put(MESSAGE_NOTE, messageNote);
        long lastInsertId = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertId;
    }


    //        Select query for update user for database
    public int updateUserById(String name, String fatherName, String surName, int gender, String birthDate,
                              String photo, String birthPlace, int isLive, String deathDate, String deathPlace, int relationId, String messageNote, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(GENDER, gender);
        cv.put(BIRTH_DATE, birthDate);
        cv.put(PHOTO, photo);
        cv.put(BIRTH_PLACE, birthPlace);
        cv.put(IS_LIVE, isLive);
        cv.put(DEATH_DATE, deathDate);
        cv.put(DEATH_PLACE, deathPlace);
        cv.put(RELATION_ID, relationId);
        cv.put(MESSAGE_NOTE, messageNote);
        int lastUpdateId = db.update(TABLE_NAME, cv, USER_ID + "=?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdateId;
    }

    //    select query  delete for delete user
    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int deleteUserId = db.delete(TABLE_NAME, USER_ID + "=?", new String[]{String.valueOf(userId)});
        db.close();
        return deleteUserId;
    }
}
