package com.aswdc_familytree.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_familytree.model.RelationModel;

import java.util.ArrayList;

public class Relation extends MyDatabase {

    public static final String TABLE_NAME = "Relation";
    public static final String RELATION_ID = "RelationId";
    public static final String RELATION_NAME = "RelationName";


    public Relation(Context context) {
        super(context);
    }

    public ArrayList<RelationModel> getRelation() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<RelationModel> list = new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        RelationModel relationModel1 = new RelationModel();
        relationModel1.setRelationName("Choose Relation");
        list.add(relationModel1);
        for (int i = 0; i < cursor.getCount(); i++) {
            RelationModel relationModel = new RelationModel();
            relationModel.setRelationId(cursor.getInt(cursor.getColumnIndex(RELATION_ID)));
            relationModel.setRelationName(cursor.getString(cursor.getColumnIndex(RELATION_NAME)));
            list.add(relationModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public RelationModel getRelationById(int relationId) {
        SQLiteDatabase db = getReadableDatabase();
        RelationModel model = new RelationModel();
        String query = "SELECT * FROM " + TABLE_NAME + " Where " + RELATION_ID + "=?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(relationId)});
        cursor.moveToFirst();
        model.setRelationId(cursor.getInt(cursor.getColumnIndex(RELATION_ID)));
        model.setRelationName(cursor.getString(cursor.getColumnIndex(RELATION_NAME)));
        cursor.close();
        db.close();
        return model;
    }
}
