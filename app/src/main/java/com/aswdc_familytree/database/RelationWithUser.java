package com.aswdc_familytree.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc_familytree.model.RelationWithUserModel;

import java.util.ArrayList;

public class RelationWithUser extends MyDatabase {

    public static final String TABLE_NAME = "RelationWithUser";
    public static final String ID = "Id";
    public static final String USER_ID = "UserId";
    public static final String RELATION_ID = "RelationId";


    public RelationWithUser(Context context) {
        super(context);
    }

    //      //    select query  All users get in table Relation
    public ArrayList<RelationWithUserModel> getRelationWithUser() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<RelationWithUserModel> list = new ArrayList<>();
//        String query = " SELECT * FROM " + TABLE_NAME;
        String query = " SELECT Relation.RelationName, " +
                " user.Name ," +
                " Relation.id " +
                " FROM " +
                " RelationWithUser " +
                "  INNER JOIN " +
                " Relation ON RelationWithUser.RelationId = Relation.Id" +
                " INNER JOIN " +
                " TblUserAdd user ON RelationWithUser.UserId = user.UserId";

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            RelationWithUserModel relationWithUserModel = new RelationWithUserModel();
            relationWithUserModel.setId(cursor.getInt(cursor.getColumnIndex(ID)));
            relationWithUserModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            relationWithUserModel.setRelationId(cursor.getInt(cursor.getColumnIndex(RELATION_ID)));
            list.add(relationWithUserModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    //    select query  Get user Relation with id
    public RelationWithUserModel getRelationWithUserById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        RelationWithUserModel model = new RelationWithUserModel();
        String query = "SELECT * FROM " + TABLE_NAME + " Where " + ID + "=?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        model.setRelationId(cursor.getInt(cursor.getColumnIndex(RELATION_ID)));
        cursor.close();
        db.close();
        return model;
    }

    //    select query  Insert User Relation
    public long insertRelationWithUser(int userId, int relation) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID, String.valueOf(userId));
        cv.put(RELATION_ID, relation);
        long lastInsertId = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertId;
    }

    public long updateRelationWithUser(int userId, int relation) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID, String.valueOf(userId));
        cv.put(RELATION_ID, relation);
//        cv.put(ID,id);
       /* int lastUpdateId = db.update(TABLE_NAME, cv, USER_ID + "=?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdateId;*/
        long lastInsertId = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertId;
    }

    //    select query  delete for delete user and Relation
    public int deleteUserRelationById(int Id) {
        SQLiteDatabase db = getWritableDatabase();
        int deleteUserId = db.delete(TABLE_NAME, ID + "=?", new String[]{String.valueOf(Id)});
        db.close();
        return deleteUserId;
    }


}
